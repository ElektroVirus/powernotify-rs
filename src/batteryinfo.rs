use std::fs;
use configparser::ini::Ini;

// const BATTERY_LEVEL_FILENAME: &str = "/sys/class/power_supply/BAT0/capacity";
const BATTERY_LEVEL_FILENAME: &str = "capacity";

// const AC_ONLINE_FILENAME: &str = "/sys/class/power_supply/AC/online";
const AC_ONLINE_FILENAME: &str = "online";

const NOTIFIED_FILENAME: &str = "notifications.ini";

pub struct Battery {
    pub level: i8,
    pub charging: bool
}

pub struct Notified {
    pub low: bool,
    pub critical: bool
}

pub fn parse_battery_info() -> Battery {
    let battery_contents = fs::read_to_string(BATTERY_LEVEL_FILENAME)
        .expect("Could not read file.");
    let ac_online_contents = fs::read_to_string(AC_ONLINE_FILENAME)
        .expect("Could not read file.");

    fn parse_bool(arg: &str) -> bool {
        match arg.trim() {
            "1" => true,
            "0" => false,
            _   => panic!(),
        }
    }

    let battery_level: i8   = battery_contents.trim().parse::<i8>().unwrap();
    let ac_online: bool     = parse_bool(&ac_online_contents);

    let bat = Battery {
        level: battery_level,
        charging: ac_online
    };
    return bat;
}

pub fn parse_notified_file() -> Notified {
    let content = fs::read_to_string(NOTIFIED_FILENAME)
        .expect("Could not read file.");

    let mut config = Ini::new();
    config.read(content);

    let notified_low = config.getbool("notified", "low").unwrap().unwrap();
    let notified_critical = config.getbool("notified", "critical").unwrap().unwrap();

    let notified = Notified {
        low: notified_low,
        critical: notified_critical
    };
    return notified;
}

fn set_low_notified() -> () {

}

