use notify_rust::Notification;

const THRESHOLD_LOW_PERSENTAGE:         i8 = 30;
const THRESHOLD_CRITICAL_PERSENTAGE:    i8 = 10;

mod batteryinfo;

fn main() {

    let bat: batteryinfo::Battery = batteryinfo::parse_battery_info();

    println!("Battery level: {}", bat.level);
    println!("Charging:      {}", bat.charging);

    let notified = batteryinfo::parse_notified_file();

    println!("notified.low:      {}", notified.low);
    println!("notified.critical: {}", notified.critical);

    /*Notification does not need to be sent if it is already sent. It is indicated by writing '1'
     * into file 'notified'. */ 
    let show_message: bool;
    let summary: String;
    let message: String = format!("Battery level {}%", bat.level);

    if bat.level < THRESHOLD_CRITICAL_PERSENTAGE && !notified.critical {
        summary = "Critical battery".to_string();
        show_message = true;
    }
    else if bat.level < THRESHOLD_LOW_PERSENTAGE && !notified.low {
        summary = "Low battery".to_string();
        show_message = true;
    }
    else {
        summary = "Battery level".to_string();
        show_message = false;
    }


    if show_message {
        Notification::new()
            .summary(&summary)
            .body(&message)
            .show().unwrap();
    }
}
